export type CommentInput = {
    author: string;
    content: string;
}

export type CommentOutput = {
    id: string,
    author: string;
    content: string;
}