import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { IdVO } from '../../../domain/vos/id.vo';

@Service()
export class DeleteCommentUseCase {

    constructor(private postService: PostService){}

    async execute(userId:string, postId:string, commentId: string){
        // try{
        const isUUID = IdVO.createWithUUID(commentId);
        await this.postService.deleteComment(userId, postId, isUUID);
        return isUUID.value;

        // } catch(error) {
        //     throw new ExceptionWithCode(400, 'Unable to delete');
        // }
    }
}