
jest.mock(
    './../../../../infrastructure/repositories/post.repository.pg',
    () => {
        return {
            PostRepositoryPG: jest.fn().mockImplementation(() => {
                return {
                    createComment: jest.fn().mockImplementation(() => {
                        return new Comment({
                            id: IdVO.create(),
                            author: NickNameVO.create('StarFlow'),
                            content: CommentContentVO.create(
                                'Im not agree at all for what you are saying'
                            ),
                        });
                    }),
                };
            }),
        };
    }
);

import 'reflect-metadata';
import Container from 'typedi';
import { Comment } from '../../../../domain/entities/comment.entity';
import { CommentContentVO } from '../../../../domain/vos/comments/date.vo';
import { IdVO } from '../../../../domain/vos/id.vo';
import { NickNameVO } from '../../../../domain/vos/nickname.vo';
import { PostRepositoryPG } from '../../../../infrastructure/repositories/post.repository.pg';
import { CommentInput } from '../comment.interface';
import { CreateCommentUseCase } from './../create-comment.usecase';

describe('Create comment use case', () => {

    it('should create a new comment into a post', async () => {
         
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);    
    
        const useCase = Container.get(CreateCommentUseCase);
        const userId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';
        const postId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';

        const input: CommentInput = {
            author: 'StarFlow',
            content: 'Im not agree at all for what you are saying'
        };

        const response = await useCase.execute(userId, postId,input );
        console.log(response);

        expect(repository.createComment).toHaveBeenCalled();
        expect(response.author).toBe('StarFlow');

    });
});