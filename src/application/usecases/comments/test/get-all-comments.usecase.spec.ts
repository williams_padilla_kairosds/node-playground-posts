jest.mock(
    './../../../../infrastructure/repositories/post.repository.pg',
    () => {
        return {
            PostRepositoryPG: jest.fn().mockImplementation(() => {
                return {
                    getAllComments: jest.fn().mockImplementation(() => {
                        return [new Comment({
                            id: IdVO.create(),
                            author: NickNameVO.create('StarFlow'),
                            content: CommentContentVO.create(
                                'Im not agree at all for what you are saying'
                            ),
                        })];
                    }),
                };
            }),
        };
    }
);

import 'reflect-metadata';
import Container from 'typedi';
import { Comment } from '../../../../domain/entities/comment.entity';
import { CommentContentVO } from '../../../../domain/vos/comments/date.vo';
import { IdVO } from '../../../../domain/vos/id.vo';
import { NickNameVO } from '../../../../domain/vos/nickname.vo';
import { PostRepositoryPG } from '../../../../infrastructure/repositories/post.repository.pg';
import { GetAllCommentsUseCase } from './../get-all-comment.usecase';
describe('Get all comments use case', () => {
    it('should get all comments', async () => {
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(GetAllCommentsUseCase);
        const userId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';
        const postId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';

        const response = await useCase.execute(userId, postId);

        expect(repository.getAllComments).toHaveBeenCalled();
        expect(response[0].author).toBe('StarFlow');
    });
});
