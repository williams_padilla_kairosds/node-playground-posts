jest.mock(
    './../../../../infrastructure/repositories/post.repository.pg',
    () => {
        return {
            PostRepositoryPG: jest.fn().mockImplementation(() => {
                return {
                    getCommentById: jest.fn().mockImplementation(() => {
                        return new Comment({
                            id: IdVO.create(),
                            author: NickNameVO.create('StarFlow'),
                            content: CommentContentVO.create(
                                'Im not agree at all for what you are saying'
                            ),
                        });
                    }),
                };
            }),
        };
    }
);

import 'reflect-metadata';
import Container from 'typedi';
import { Comment } from '../../../../domain/entities/comment.entity';
import { CommentContentVO } from '../../../../domain/vos/comments/date.vo';
import { IdVO } from '../../../../domain/vos/id.vo';
import { NickNameVO } from '../../../../domain/vos/nickname.vo';
import { PostRepositoryPG } from '../../../../infrastructure/repositories/post.repository.pg';
import { GetCommentByIdUseCase } from './../get-by-id-comment.usecase';
describe('Get comment by id use case', () => {
    it('should get a comment by its id', async () => {
        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(GetCommentByIdUseCase);
        const userId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';
        const postId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';
        const commentId = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';

        const response = await useCase.execute(userId, postId, commentId);
        console.log(response);

        expect(repository.getCommentById).toHaveBeenCalled();
        expect(response.author).toBe('StarFlow');
    });
});
