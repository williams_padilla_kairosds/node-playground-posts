import { Service } from 'typedi';
import { OffensiveWordService } from '../../domain/services/offensive-word.service';
import { IdVO } from '../../domain/vos/id.vo';
import { OffensiveWordResponse } from './offensive-word.response';


@Service()
export class GetOffensiveWordByIdUseCase {
    
    constructor(private offensiveWordService: OffensiveWordService) {}

    async execute(id: string): Promise<OffensiveWordResponse>{
        const isUUID = IdVO.createWithUUID(id);
        const response = await this.offensiveWordService.getById(isUUID);
        const offensiveWordDTO: OffensiveWordResponse = {
            id: response.id,
            word: response.word,
            level: response.level
        };
        return offensiveWordDTO;
    }
}