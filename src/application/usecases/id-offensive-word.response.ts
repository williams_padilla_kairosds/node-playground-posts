import { IdVO } from '../../domain/vos/id.vo';

export type IdOffensiveWordResponse = {
    id: IdVO
}