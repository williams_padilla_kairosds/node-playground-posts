import { Service } from 'typedi';
import { PostType } from '../../../domain/entities/post.entity';
import { PostService } from '../../../domain/services/post.service';
import { TextVO } from '../../../domain/vos/author/text.vo';
import { TitleVO } from '../../../domain/vos/author/title.vo';
import { IdVO } from '../../../domain/vos/id.vo';
import { NickNameVO } from '../../../domain/vos/nickname.vo';
import { PostRequest, PostResponse } from './post.interfaces';

@Service()
export class CreatePostUseCase {

    constructor(private postService:PostService) {}

    async execute(userId : string,postRequest:PostRequest): Promise<PostResponse> {

        
        const newPost: PostType = {
            id: IdVO.create(),
            authorName:NickNameVO.create(postRequest.author) ,
            text: TextVO.create(postRequest.text) ,
            title: TitleVO.create(postRequest.title) 
        };

        
        const response = await this.postService.create(userId,newPost);

        const sendResponse:PostResponse = {
            id: response.id,
            author: response.authorName,
            title: response.title,
            text: response.text
        };
        
        return sendResponse;
    }
}