import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { IdVO } from '../../../domain/vos/id.vo';

@Service()
export class DeletePostUseCase {

    constructor(private postService: PostService){}

    async execute(userId:string, id:string): Promise<string> {
        const isUUID = IdVO.createWithUUID(id);
        await this.postService.delete(userId, isUUID);
        return isUUID.value;
    }
}