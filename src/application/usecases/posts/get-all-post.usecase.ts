import { Service } from 'typedi';
import { PostService } from '../../../domain/services/post.service';
import { PostResponse } from './post.interfaces';


@Service()
export class GetAllPostsUseCase {

    constructor(private postService: PostService){}

    async execute(userId: string):Promise<PostResponse[]> {
        const response = await this.postService.getAll(userId);
        const postResponse:PostResponse[] = response.map(post => {
            return {
                id: post.id,
                author: post.authorName,
                title: post.title,
                text: post.text,
            };
        });
        return postResponse;
    }
}