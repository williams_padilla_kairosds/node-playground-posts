jest.mock('./../../../../infrastructure/repositories/post.repository.pg', () => {
    return {
        PostRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                createPost: jest.fn().mockImplementation(() => {
                    return new Post({
                        id: IdVO.create(),
                        authorName: NickNameVO.create('StarFlow'),
                        title: TitleVO.create('Hola como estas'),
                        text: TextVO.create(
                            'I was wondeasdasdasdasdasdasdawwdasdwqfqfqwfqwfqwfqfqwfqwfqwfqwfwq...'
                        ),
                    });
                })
            };
        })
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../../domain/entities/post.entity';
import { TextVO } from '../../../../domain/vos/author/text.vo';
import { TitleVO } from '../../../../domain/vos/author/title.vo';
import { IdVO } from '../../../../domain/vos/id.vo';
import { NickNameVO } from '../../../../domain/vos/nickname.vo';
import { PostRequest } from '../post.interfaces';
import { PostRepositoryPG } from './../../../../infrastructure/repositories/post.repository.pg';
import { CreatePostUseCase } from './../create-post.usecase';
describe('Create post use case', () => {

    it('should create a post and persist', async () => {

        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(CreatePostUseCase);
        const id = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';
        const input: PostRequest = {
            author: 'StaFlow',
            title: 'Hola como estas',
            text: 'I was wondeasdasdasdasdasdasdawwdasdwqfqfqwfqwfqwfqfqwfqwfqwfqwfwq...',
        }; 
        const post = await useCase.execute(id, input);

        expect(repository.createPost).toHaveBeenCalled();
        expect(post.author).toBe('StarFlow');
    });
});