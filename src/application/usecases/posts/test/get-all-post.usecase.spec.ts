jest.mock('../../../../infrastructure/repositories/post.repository.pg', () => {
    return {
        PostRepositoryPG: jest.fn().mockImplementation(() => {
            return {
                getAllPost: jest.fn().mockImplementation(() => {
                    return [
                        new Post({
                            id: IdVO.create(),
                            authorName: NickNameVO.create('StarFlow'),
                            title: TitleVO.create('Hi how are you'),
                            text: TextVO.create(
                                'I was wondeasdasdasdasdasdasdawwdasdwqfqfqwfqwfqwfqfqwfqwfqwfqwfwq...'
                            ),
                        }),
                    ];
                })
            };
        })
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../../domain/entities/post.entity';
import { TextVO } from '../../../../domain/vos/author/text.vo';
import { TitleVO } from '../../../../domain/vos/author/title.vo';
import { IdVO } from '../../../../domain/vos/id.vo';
import { NickNameVO } from '../../../../domain/vos/nickname.vo';
import { PostRepositoryPG } from '../../../../infrastructure/repositories/post.repository.pg';
import { GetAllPostsUseCase } from './../get-all-post.usecase';

describe(' Get all post use case', () => {

    it('should return all posts', async () => {

        const repository = new PostRepositoryPG();
        Container.set('PostRepository', repository);

        const useCase = Container.get(GetAllPostsUseCase);
        const id = '1cca625c-0502-42af-95b8-9f6ab6a9be7f';

        const posts = await useCase.execute(id);

        expect(repository.getAllPost).toHaveBeenCalled();
        expect(posts[0].title).toBe('Hi how are you');
    });
});