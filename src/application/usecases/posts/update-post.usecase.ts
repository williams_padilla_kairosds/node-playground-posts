import { Service } from 'typedi';
import { PostType } from '../../../domain/entities/post.entity';
import { PostService } from '../../../domain/services/post.service';
import { TextVO } from '../../../domain/vos/author/text.vo';
import { TitleVO } from '../../../domain/vos/author/title.vo';
import { IdVO } from '../../../domain/vos/id.vo';
import { NickNameVO } from '../../../domain/vos/nickname.vo';
import { PostRequest, PostResponse } from './post.interfaces';


@Service()
export class UpdatePostUseCase {
    constructor(private postService: PostService){}

    async execute(userId:string, postId: string, postRequest: PostRequest): Promise< PostResponse> {
        const request: PostType = {
            id: IdVO.createWithUUID(postId),
            authorName: NickNameVO.create(postRequest.author),
            title: TitleVO.create(postRequest.title),
            text: TextVO.create(postRequest.text)
        };

        const response = await this.postService.update(userId, request);

        const sentResponse: PostResponse = {
            id: response.id,
            author: response.authorName,
            title: response.title,
            text: response.text
        };

        return sentResponse;
    }
}