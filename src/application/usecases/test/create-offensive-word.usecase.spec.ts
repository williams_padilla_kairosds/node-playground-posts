jest.mock('./../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
    return {
        OffensiveWordRepositoryMongo: jest.fn().mockImplementation(()=> {
            return {
                save: jest.fn().mockImplementation(()=> {
                    return new OffensiveWord({
                        id: IdVO.create(),
                        word: WordVO.create('cacota'),
                        level: LevelVO.create(5)
                    });
                })
            };
        })
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/entities/offensive-word.entity';
import { IdVO } from '../../../domain/vos/id.vo';
import { LevelVO } from '../../../domain/vos/level.vo';
import { WordVO } from '../../../domain/vos/word.vo';
import { CreateOffensiveWordUseCase } from '../create-offensive-word.usecase';
import { OffensiveWordRequest } from '../offensive-word.request';
import { OffensiveWordRepositoryMongo } from './../../../infrastructure/repositories/offensive-word.repository.mongo';
describe('Create ofensive word use case', () => {

    it('it should create offensive word and persist', async () => {

        const repository = new OffensiveWordRepositoryMongo();
        Container.set('OffensiveWordRepository', repository);

        const useCase = Container.get(CreateOffensiveWordUseCase);
        const offensiveWordRequest: OffensiveWordRequest = {
            word:'cacota',
            level: 5
        };
        const offensiveWord = await useCase.execute(offensiveWordRequest);
        console.log('hola',offensiveWord);
        expect(repository.save).toHaveBeenCalled();
        expect(offensiveWord.level).toBe(5);



    });
});