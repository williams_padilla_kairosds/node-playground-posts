jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo');

import 'reflect-metadata';
import Container from 'typedi';
import { IdVO } from '../../../domain/vos/id.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { DeleteOffensiveWordUseCase } from '../delete-offensive-word.usecase';

describe('Delete offensive word use case', () => {

    it('should delete offensive word by id', async () => {

        const repository = new OffensiveWordRepositoryMongo();
        Container.set('OffensiveWordRepository', repository);

        const useCase = Container.get(DeleteOffensiveWordUseCase);
        const id = IdVO.create().value;
        await useCase.execute(id);

        expect(repository.delete).toHaveBeenCalled();
    });
});