jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
    return {
        OffensiveWordRepositoryMongo: jest.fn().mockImplementation(()=> {
            return {
                getOffensiveWords: jest.fn().mockImplementation(()=> {
                    return [new OffensiveWord({
                        id: IdVO.create(),
                        word: WordVO.create('cacota'),
                        level:LevelVO.create(5)
                    })];
                })
            };
        })
    };
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/entities/offensive-word.entity';
import { IdVO } from '../../../domain/vos/id.vo';
import { LevelVO } from '../../../domain/vos/level.vo';
import { WordVO } from '../../../domain/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { GetAllOffensiveWordsUseCase } from './../get-all-offensive-words.usecase';
describe('Get all offensive words use case', () => {

    it('should return all offensive words', async () => {

        const repository = new OffensiveWordRepositoryMongo();
        Container.set('OffensiveWordRepository', repository);

        const useCase = Container.get(GetAllOffensiveWordsUseCase);
        const offensiveWord = await useCase.execute();

        console.log(offensiveWord);
        expect(repository.getOffensiveWords).toHaveBeenCalled();
        expect(offensiveWord[0].word).toBe('cacota');
        expect(offensiveWord[0].level).toBe(5);

    });
});