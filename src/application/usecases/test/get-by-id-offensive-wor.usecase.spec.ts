
jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
    return {

        OffensiveWordRepositoryMongo: jest.fn().mockImplementation(()=> {
            return {
                getById: jest.fn().mockImplementation(()=> {
                    return new OffensiveWord({
                        id: IdVO.create(),
                        word: WordVO.create('shit'),
                        level: LevelVO.create(4)
                    });
                })
            };
        })
    }; 
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/entities/offensive-word.entity';
import { IdVO } from '../../../domain/vos/id.vo';
import { LevelVO } from '../../../domain/vos/level.vo';
import { WordVO } from '../../../domain/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { GetOffensiveWordByIdUseCase } from './../get-by-id-offensive-word.usecase';
describe('Get offensive word by id use case', () => {

    it('should get an offensive word by id', async () => {

        const repository = new OffensiveWordRepositoryMongo();
        Container.set('OffensiveWordRepository', repository);

        const useCase = Container.get(GetOffensiveWordByIdUseCase);
        const isUUID = IdVO.create().value;

        const response = await useCase.execute(isUUID);

        expect(repository.getById).toHaveBeenCalled();
        expect(response.word).toBe('shit');
        expect(response.level).toBe(4);
    });
});