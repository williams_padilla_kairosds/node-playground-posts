import { Service } from 'typedi';
import { OffensiveWordType } from '../../domain/entities/offensive-word.entity';
import { OffensiveWordService } from '../../domain/services/offensive-word.service';
import { IdVO } from '../../domain/vos/id.vo';
import { LevelVO } from '../../domain/vos/level.vo';
import { WordVO } from '../../domain/vos/word.vo';
import { OffensiveWordRequest } from './offensive-word.request';
import { OffensiveWordResponse } from './offensive-word.response';

@Service()
export class UpdateOffensiveWordUseCase {

    constructor(private offensiveWordService: OffensiveWordService){}

    async execute(id: string, offensiveWordRequest: OffensiveWordRequest): Promise<OffensiveWordResponse> {
        const offensiveWordData: OffensiveWordType = {
            id: IdVO.createWithUUID(id),
            word: WordVO.create(offensiveWordRequest.word),
            level: LevelVO.create(offensiveWordRequest.level),
        };
        const response = await this.offensiveWordService.update(offensiveWordData.id, offensiveWordData);
        const offensiveWordDto: OffensiveWordResponse= {
            id: response.id,
            word: response.word,
            level: response.level
        };
        return offensiveWordDto;
    }
}