import { TextVO } from '../vos/author/text.vo';
import { TitleVO } from '../vos/author/title.vo';
import { IdVO } from '../vos/id.vo';
import { NickNameVO } from '../vos/nickname.vo';

export type PostType = {
    id: IdVO;
    authorName: NickNameVO;
    title: TitleVO;
    text: TextVO;
    // comment: Comment[]
}

export class Post{
    constructor(private post: PostType){}

    get id(): string{
        return this.post.id.value;
    }

    get authorName(): string{
        return this.post.authorName.value;
    }

    get title(): string {
        return this.post.title.value;
    }

    get text(): string {
        return this.post.text.value;
    }

    // get comment(): Array<Comment>{
    //     return this.post.comment;
    // }
}