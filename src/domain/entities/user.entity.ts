import { EmailVo } from '../vos/email.vo';
import { IdVO } from '../vos/id.vo';
import { NickNameVO } from '../vos/nickname.vo';
import { PasswordVo } from '../vos/password.entity';
import { RoleVo } from '../vos/role.vo';

export type UserType = {
    id: IdVO;
    email: EmailVo;
    password: PasswordVo;
    role: RoleVo;
    nickName: NickNameVO

}

export class User{

    constructor(private user: UserType){}

    get id(): IdVO {
        return this.user.id;
    }

    get email(): EmailVo {
        return this.user.email;
    }

    get password(): PasswordVo {
        return this.user.password;
    }

    get role(): RoleVo {
        return this.user.role;
    }

    get nickName(): NickNameVO {
        return this.user.nickName;
    }

}