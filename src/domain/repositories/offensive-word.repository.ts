import { OffensiveWord } from '../entities/offensive-word.entity';
import { IdVO } from '../vos/id.vo';

export interface OffensiveWordRepository{
    save(offensiveWord: OffensiveWord): Promise<OffensiveWord>;

    getOffensiveWords(): Promise<OffensiveWord[]>

    delete(id: IdVO): Promise<void>;

    update(id: IdVO, OffensiveWord:OffensiveWord): Promise<OffensiveWord>
    
    getById(id: IdVO): Promise<OffensiveWord>
    
}