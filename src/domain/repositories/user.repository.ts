import { User } from '../entities/user.entity';
import { EmailVo } from '../vos/email.vo';

export interface UserRepository {

    save(user: User): Promise<void> 

    getByEmail(email:EmailVo): Promise<User | null>

    
}