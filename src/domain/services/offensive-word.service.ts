import { Inject, Service } from 'typedi';
import { OffensiveWord, OffensiveWordType } from '../entities/offensive-word.entity';
import { ExceptionWithCode } from '../execption-with-code';
import { OffensiveWordRepository } from '../repositories/offensive-word.repository';
import { IdVO } from '../vos/id.vo';

@Service()
export class OffensiveWordService {
    constructor(
        @Inject('OffensiveWordRepository')
        private offensiveWordRepository: OffensiveWordRepository
    ) {}

    async persist(offensiveWord: OffensiveWordType): Promise<OffensiveWord> {
        const offensiveWordEntity = new OffensiveWord(offensiveWord);
        return await this.offensiveWordRepository.save(offensiveWordEntity);
    }

    async getAll(): Promise<OffensiveWord[]> {
        return await this.offensiveWordRepository.getOffensiveWords();
    }

    async deleteById(id: IdVO): Promise<void> {
        await this.checkIfIdExist(id);
        await this.offensiveWordRepository.delete(id);
    }

    async update(
        id: IdVO,
        offensiveWord: OffensiveWordType
    ): Promise<OffensiveWord> {
        await this.checkIfIdExist(id);
        const offensiveWordEntity = new OffensiveWord(offensiveWord);
        console.log('entity en servicio:',offensiveWordEntity);

        return await this.offensiveWordRepository.update(
            id,
            offensiveWordEntity
        );
    }

    async getById(id: IdVO): Promise<OffensiveWord> {
        return await this.offensiveWordRepository.getById(id);
    }

    private async checkIfIdExist(id: IdVO): Promise<void> {
        try {
            await this.getById(id);
        } catch (error) {
            throw new ExceptionWithCode(404, `Id ${id.value} not found`);
        }
    }
}