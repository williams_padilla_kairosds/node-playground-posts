import { Inject, Service } from 'typedi';
import { Comment, CommentType } from '../entities/comment.entity';
import { Post, PostType } from '../entities/post.entity';
import { ExceptionWithCode } from '../execption-with-code';
import { PostRepository } from '../repositories/post.repository';
import { IdVO } from '../vos/id.vo';

@Service()
export class PostService {
    constructor(
        @Inject('PostRepository') private postRepository: PostRepository
    ) {}

    async create(userId: string, postType: PostType): Promise<Post> {
        return await this.postRepository.createPost(userId, new Post(postType));
    }

    async getAll(userId: string): Promise<Post[]> {
        return await this.postRepository.getAllPost(userId);
    }

    async getById(userId: string, id: IdVO): Promise<Post> {
        return await this.postRepository.getById(userId, id);
    }

    private async checkIfIdExist(userId: string, id: IdVO): Promise<void> {
        console.log('id en checker exist', id);
        try {
            await this.getById(userId, id);
        } catch (error) {
            throw new ExceptionWithCode(
                400,
                `Post Id ${id.value} is not found`
            );
        }
    }

    async delete(userId: string, id: IdVO): Promise<IdVO> {
        console.log('id en delete service', id);
        await this.checkIfIdExist(userId, id);
        await this.postRepository.delete(userId, id);
        return id;
    }

    async update(userId: string, post: PostType): Promise<Post> {
        await this.checkIfIdExist(userId, post.id);
        return await this.postRepository.updatePost(userId, new Post(post));
    }

    //-----------------> Comments Crud services <----------------------//

    async createComment(
        userId: string,
        postId: string,
        comment: CommentType
    ): Promise<Comment> {
        return await this.postRepository.createComment(
            userId,
            postId,
            new Comment(comment)
        );
    }

    async getAllComments(userId: string, postId: string): Promise<Comment[]> {
        return await this.postRepository.getAllComments(userId, postId);
    }

    async getCommentById(
        userId: string,
        postId: string,
        id: IdVO
    ): Promise<Comment> {
        return await this.postRepository.getCommentById(userId, postId, id);
    }

    async deleteComment(
        userId: string,
        postId: string,
        commentId: IdVO
    ): Promise<IdVO> {
        await this.checkIfCommentIdExist(userId, postId, commentId);
        return await this.postRepository.deleteComment(
            userId,
            postId,
            commentId
        );
    }

    async updateComment(userId: string, postId:string,  comment: CommentType): Promise<Comment>{
        await this.checkIfCommentIdExist(userId, postId, comment.id );
        return await this.postRepository.updateComment(userId, postId, new Comment(comment));
    }

    private async checkIfCommentIdExist(
        userId: string,
        postId: string,
        id: IdVO
    ) {
        try {
            await this.getCommentById(userId, postId, id);
        } catch (error) {
            throw new ExceptionWithCode(
                400,
                `Comment id ${id.value} is not found`
            );
        }
    }
}
