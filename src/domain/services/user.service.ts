import bcrypt from 'bcrypt';
import { Inject, Service } from 'typedi';
import { User, UserType } from '../entities/user.entity';
import { UserRepository } from '../repositories/user.repository';
import { EmailVo } from '../vos/email.vo';
import { PasswordVo } from '../vos/password.entity';

@Service()
export class UserService {

    constructor(@Inject ('UserRepository') private userRepository: UserRepository){}

    async isValidPassword(password: PasswordVo, user:User ): Promise<boolean> {
        return await bcrypt.compare(password.value, user.password.value);
    }

    async persist(user: User): Promise<void>{
        const encryptPassword = PasswordVo.createWithHash(user.password.value);
        const newUser: UserType = {
            id:user.id,
            email: user.email,
            password: encryptPassword,
            role: user.role,
            nickName: user.nickName
        };
        await this.userRepository.save(new User(newUser));
    }

    async getByEmail(email: EmailVo): Promise<User | null>{
        return await this.userRepository.getByEmail(email);
    }



}
