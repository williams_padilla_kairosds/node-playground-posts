import { TextVO } from './text.vo';

describe('Text VO', () => {

    it('should create', () => {
        const comment =
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In mattis.';
        const text = TextVO.create(comment);

        expect(text.value).toEqual(comment);
    });

    it('should throw error if text length is greater than 300 characters', () => {
        const text =
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis ullamcorper velit. Integer nec nisi et erat vestibulum tempus. Pellentesque vel urna in dolor ullamcorper maximus. Proin vel dolor faucibus, blandit sem at, cursus ante. Mauris finibus condimentum ipsum. Pellentesque sagittis condimentum nunc quis tincidunt. Suspendisse at tellus pulvinar, dictum.';

        expect(() => TextVO.create(text)).toThrow(
            `${text} is not valid, It length must be between 50 and 300`
        );
    });

    it('should throw error if text length is shorter than 300 characters', () => {
        const text = 'Im not sure';
        expect(() => TextVO.create(text)).toThrow(
            `${text} is not valid, It length must be between 50 and 300`
        );
    });
});