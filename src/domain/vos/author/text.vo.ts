import { ExceptionWithCode } from '../../execption-with-code';

export class TextVO {

    get value(): string {
        return this.text;
    }

    private constructor(private text: string){}

    static create(text:string): TextVO {
        if (text.length < 50 || text.length > 300) {
            throw new ExceptionWithCode(400,
                `${text} is not valid, It length must be between 50 and 300`
            );
        }

        return new TextVO(text);
    }
}