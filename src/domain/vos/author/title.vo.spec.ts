import { TitleVO } from './title.vo';

describe('Title VO', () => {
    it('should create', () => {
        const name = 'My Effort';
        const title = TitleVO.create(name);
        expect(title.value).toEqual(name);
    });

    it('should throw an error when the title length is greater than 30', () => {
        const title =
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tempor.';

        expect(() => TitleVO.create(title)).toThrow(
            `${title} is not valid, It length must be between 5 and 30`
        );
    });

    it('should throw an error when the title length is shorter than 5', () => {
        const title = 'Hi';

        expect(() => TitleVO.create(title)).toThrow(
            `${title} is not valid, It length must be between 5 and 30`
        );
    });

});
