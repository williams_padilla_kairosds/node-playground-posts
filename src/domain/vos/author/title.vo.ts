export class TitleVO{

    get value(): string {
        return this.title;
    }

    private constructor(private title: string){}

    static create(title:string): TitleVO {
        if (title.length < 5 || title.length > 30) {
            throw new Error(
                `${title} is not valid, It length must be between 5 and 30`
            );
        }

        return new TitleVO(title);
    }
}