import { ExceptionWithCode } from '../../execption-with-code';

export class CommentContentVO {

    get value(): string {
        return this.commentContent;

    }

    private constructor(private commentContent: string)  {}

    static create(commentContent: string): CommentContentVO{
        if (commentContent.length < 5 || commentContent.length > 300) {
            throw new ExceptionWithCode(
                400,
                `${commentContent} is not valid, It length must be between 5 and 300`
            );
        }

        return new CommentContentVO(commentContent);

    }
}