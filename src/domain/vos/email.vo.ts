import { ExceptionWithCode } from '../execption-with-code';

export class EmailVo{

    get value(): string{
        return this.email;
    }

    private constructor(private email: string) {}

    static create(email: string): EmailVo {
        if(!email) throw new ExceptionWithCode(400, `${email} is not a valid email`);
        return new EmailVo(email);
    }
}