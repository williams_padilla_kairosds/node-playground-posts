import { ExceptionWithCode } from '../execption-with-code';

export class NickNameVO {
    get value(): string {
        return this.nickname;
    }

    private constructor(private nickname: string) {}

    static create(nickname: string): NickNameVO {
        if (nickname.length < 3 || nickname.length > 15) {
            throw new ExceptionWithCode(
                400,
                `${nickname} is not valid. Nickname must be between 3 and 15 characters`
            );
        }
        return new NickNameVO(nickname);
    }
}
