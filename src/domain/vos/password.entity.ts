import bcrypt from 'bcrypt';
import { ExceptionWithCode } from '../execption-with-code';

export class PasswordVo {

    get value() {
        return this.password; 
    }
    private constructor(private password: string){}

    static create(password:  string): PasswordVo {
        if(!password) throw new ExceptionWithCode(400, `${password}, is not valida password`);
        return new PasswordVo(password);
    }

    static createWithHash(password:string): PasswordVo {
        const hash =  bcrypt.hashSync(password, 10);
        return new PasswordVo( hash);
    }


}