import { ExceptionWithCode } from '../execption-with-code';

export class RoleVo {

    get value():Role{
        return this.role;
    }

    private constructor(private role:Role){}

    static create(role: Role):RoleVo{
        if(!role) throw new ExceptionWithCode(400, `${role} not found`);
        return new RoleVo(role);
    }
}

export enum Role {
    ADMIN = 'ADMIN',
    USER = 'USER',
    AUTHOR = 'AUTHOR'
}