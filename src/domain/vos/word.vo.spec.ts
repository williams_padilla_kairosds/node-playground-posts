
import { WordVO } from './word.vo';

describe('Word VO', () => {

    it('should create', () => {
        const name = 'Hello';
        const word = WordVO.create(name);

        expect(word.value).toEqual(name);
    });
} );