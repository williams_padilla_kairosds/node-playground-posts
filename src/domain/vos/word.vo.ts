import { ExceptionWithCode } from '../execption-with-code';

export class WordVO{
    get value(): string{
        return this.word;
    }

    private constructor(private word: string){}

    static create(word: string): WordVO{
        if(!word) throw new ExceptionWithCode(400, `${word} is not a valid word`);
        return new WordVO(word);
    }
}