import mongoose from 'mongoose';

const connectToDB = async () => {
    const host = process.env.MONGO_HOST ?? 'localhost';
    const port = process.env.MONGO_PORT ?? '27018';
    const dbName = process.env.MONGO_DB ?? 'blog';
    try {
        await mongoose.connect(`mongodb://${host}:${port}/${dbName}`, {
            authSource: 'admin',
            auth: {
                username: process.env.MONGO_INITDB_ROOT_USERNAME ?? 'admin',
                password: process.env.MONGO_INITDB_ROOT_PASSWORD ?? 'admin',
            },
        });
    } catch (error) {
        console.log(error);
    }
};

export { connectToDB };
