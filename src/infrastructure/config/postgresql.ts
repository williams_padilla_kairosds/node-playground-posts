import { Sequelize } from 'sequelize';
import { populateDataBases } from './populate';

const host = process.env.POSTGRES_HOST ?? 'localhost';

const sequelize = new Sequelize(`postgres://pguser:pguser@${host}:5432/pgdb`);

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection Success');
    })
    .catch((err) => console.log(err));

sequelize.sync({ force: true }).then(() => {
    console.log('Database and table created');
    populateDataBases();
});

export default sequelize;
