import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import Container from 'typedi';
import { UserService } from '../../domain/services/user.service';
import { EmailVo } from '../../domain/vos/email.vo';

const opts:StrategyOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'secret' // tiene coincidir con el que hemos creado
};

export default new Strategy(opts, async (payload,done) => {
    try {
        const {email} = payload;
        const userServices = Container.get(UserService);
        const user = await userServices.getByEmail(EmailVo.create(email));

        if(user) return done(null, user);
        return done(null, false, {messsage: 'user not found'});
    } catch (error) {  
        console.log(error);
        
    }
});