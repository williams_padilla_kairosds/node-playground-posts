import { CommentModel } from '../repositories/comment.schema';
import { PostModel } from '../repositories/post.schema';
import { UserModel } from '../repositories/user.schema';

export const userRelation = () => {
    UserModel.hasMany(PostModel);
    PostModel.belongsTo(UserModel, {
        foreignKey: {
            allowNull: false,
        },
    });

    console.log(
        '--------------------------> User -- Post //// Relations created <---------------------------'
    );

    UserModel.hasMany(CommentModel);
    CommentModel.belongsTo(UserModel, {
        foreignKey: {
            allowNull: false,
        },
    });

    PostModel.hasMany(CommentModel);
    CommentModel.belongsTo(PostModel, {
        foreignKey: {
            allowNull: false,
        },
    });

    console.log(
        '--------------------------> User <--> comment <--> post //// Relations created <---------------------------'
    );
}; 
