import { DataTypes } from 'sequelize';
import sequelize from '../config/postgresql';

const CommentModel = sequelize.define('comments', {
    id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true
    },
    author: {
        type: DataTypes.STRING,
        allowNull: false
    },
    content: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

export { CommentModel };
