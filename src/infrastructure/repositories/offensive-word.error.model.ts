export class CustomError {
    constructor(public message: string, public status: number){}

}