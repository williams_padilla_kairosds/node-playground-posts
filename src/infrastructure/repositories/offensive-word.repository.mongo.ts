import { OffensiveWord, OffensiveWordType } from '../../domain/entities/offensive-word.entity';
import { OffensiveWordRepository } from '../../domain/repositories/offensive-word.repository';
import { IdVO } from '../../domain/vos/id.vo';
import { LevelVO } from '../../domain/vos/level.vo';
import { WordVO } from '../../domain/vos/word.vo';
import { OffensiveWordModel } from './offensive-word.schema';

export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {

    
    async save(offensiveWord: OffensiveWord): Promise<OffensiveWord> {
        const newOffensiveWord = {
            idOf: offensiveWord.id,
            wordOf: offensiveWord.word,
            levelOf: offensiveWord.level,
        };
        const offensiveWordModel =  new OffensiveWordModel(newOffensiveWord);
        await offensiveWordModel.save();

        const newOffensiveWordType: OffensiveWordType = {
            id: IdVO.createWithUUID(offensiveWordModel?.idOf),
            word: WordVO.create(offensiveWordModel?.wordOf),
            level: LevelVO.create(offensiveWordModel?.levelOf),
        };

        return new OffensiveWord(newOffensiveWordType);

    }

    async getById(id: IdVO): Promise<OffensiveWord> {
        
        const response = await OffensiveWordModel.findOne({idOf: id.value});

        if(!response) throw new Error('Invalid Values');
        
        const offensiveWordTyped: OffensiveWordType = {
            id: IdVO.createWithUUID(response?.idOf),
            word: WordVO.create(response?.wordOf),
            level: LevelVO.create(response?.levelOf)
        };
        return new OffensiveWord(offensiveWordTyped);
    }

    async getOffensiveWords(): Promise<OffensiveWord[]> {
        const response = await OffensiveWordModel.find();
        const offensiveWordMapped = response.map((oW) => {
            const newOffensiveWord: OffensiveWordType = { 
                id: IdVO.createWithUUID(oW.idOf) , 
                word: WordVO.create(oW.wordOf) , 
                level: LevelVO.create(oW.levelOf ) 
            };
            const offensiveWord = new OffensiveWord(newOffensiveWord);
            return offensiveWord ;
        });
        return offensiveWordMapped;
    }

    async delete(idOffensiveWord: IdVO): Promise<void> {
        try {
            
            const response = await OffensiveWordModel.findOneAndDelete({ idOf: idOffensiveWord.value });
            console.log(response);
        } catch(err){
            return console.log(err);
        }
    }

    async update( id: IdVO, offensiveWord: OffensiveWord): Promise<OffensiveWord> {

        console.log('mongo repo:', id.value);

        const newOffensiveWord = {
            wordOf: offensiveWord.word,
            levelOf: offensiveWord.level,
        };
        console.log('newoffensive word mongo repo:', newOffensiveWord);


        const response = await OffensiveWordModel.findOneAndUpdate({ idOf: id.value },newOffensiveWord,{ new:true });
        if(!response){
            throw new Error('values cant be null');
        }
        const newOffensiveWordType:OffensiveWordType = {
            id: IdVO.createWithUUID(response?.idOf),    
            word: WordVO.create(response?.wordOf),
            level: LevelVO.create(response?.levelOf)
        };
            
        return new OffensiveWord(newOffensiveWordType);
    }
}

