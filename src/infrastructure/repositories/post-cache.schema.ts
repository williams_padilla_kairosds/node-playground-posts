import mongoose from 'mongoose';

const PostCacheSchema = new mongoose.Schema({
    id: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
});

const PostCacheModel = mongoose.model('PostCache', PostCacheSchema);

export { PostCacheModel };
