import { User, UserType } from '../../domain/entities/user.entity';
import { UserRepository } from '../../domain/repositories/user.repository';
import { EmailVo } from '../../domain/vos/email.vo';
import { IdVO } from '../../domain/vos/id.vo';
import { NickNameVO } from '../../domain/vos/nickname.vo';
import { PasswordVo } from '../../domain/vos/password.entity';
import { RoleVo } from '../../domain/vos/role.vo';
import { UserModel } from './user.schema';

export class UserRepositoryPG implements UserRepository {

    async save(user: User): Promise<void> {
        const id = user.id.value;
        const email = user.email.value;
        const password = user.password.value;
        const role = user.role.value;
        const nickName = user.nickName.value;
        
        

        const userModel = UserModel.build({ id, email, password, role, nickName});
        await userModel.save();
    }

    async getByEmail(email: EmailVo): Promise<User | null> {
        const user: any = await UserModel.findOne({where: {email: email.value}});
        if(!user) return null;
        
        const userData:UserType = {
            id: IdVO.createWithUUID(user.id),
            email: EmailVo.create(user.email),
            password: PasswordVo.create(user.password),
            role: RoleVo.create(user.role),
            nickName: NickNameVO.create(user.nickName)
            
        };

        return new User(userData);
    }
}